﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElasticSearch.Client;

namespace LogStashPOC.Controllers
{
    public class HomeController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);        

        public ActionResult Index()
        {
            logger.Debug("Index Here is a debug log." + DateTime.Now.ToString());
            logger.Info("Index and an Info log." + DateTime.Now.ToString());
            logger.Warn("Index and a warning." + DateTime.Now.ToString());
            logger.Error("Index and an error." + DateTime.Now.ToString());
            logger.Fatal("Index and a fatal error." + DateTime.Now.ToString());

            return View();
        }

        public ActionResult About()
        {
            logger.Debug("About View Debbuggin Start");
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            logger.Debug("Contact View Debbuggin Start");
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}